const ICOPool = artifacts.require("./ICOPool.sol");
const TestCoin = artifacts.require("./TestCoin.sol");
const TestCoinCrowdsale = artifacts.require("./TestCoinCrowdsale.sol");

module.exports = function(deployer) {
    const startTime = web3.eth.getBlock(web3.eth.blockNumber).timestamp+1;
    const endTime = startTime + 86400;
    const rate = 500; // tokens for 1 ether
    const tokenOwner = web3.eth.accounts[2];
    const icoOwner = web3.eth.accounts[1];

    deployer.deploy(ICOPool, icoOwner);
    deployer.deploy(TestCoin);
    deployer.deploy(TestCoinCrowdsale, startTime, endTime, rate, web3.toWei(100, "ether") ,web3.toWei(1000, "ether"),  tokenOwner);
};
