pragma solidity ^0.4.18;

import "zeppelin-solidity/contracts/ownership/Ownable.sol";
import "zeppelin-solidity/contracts/crowdsale/Crowdsale.sol";
import "zeppelin-solidity/contracts/math/SafeMath.sol";

/*
    Test contract
    Implements CRUD, Ownable and buying coins via ICOPool
*/

contract ICOPool is Ownable {

    using SafeMath for uint256;

    uint8 constant fee = 1;

    mapping (uint256=>ICOInfo) private campaigns;
    uint256[] private campaignIndex;

    struct ICOInfo {
        uint256 index;
        uint256 rate;
        address crowdsaleAddress;
        string name;
        mapping(address=>uint256) balances;
        uint256 startTime;
        uint256 endTime;
    }

    event CampaignWasAdded(uint256 campaignId);
    event CampaignWasDeleted(uint256 campaignId);
    event CampaignStartTimeWasUpdated(uint256 campaignId, uint256 newTime);
    event CampaignEndTimeWasUpdated(uint256 campaignId, uint256 newTime);

    // Require msg.sender to be crowdsale owner
    // Use for new campaigns
    // todo: check for errors
    modifier onlyNewOwner(address crowdsaleAddress) {
        Crowdsale crowdsale = Crowdsale(crowdsaleAddress);
        require(msg.sender == crowdsale.token().owner() ||
                msg.sender == owner);
        _;
    }

    // Overloaded modifier for campaign id
    // Use for existing campaigns
    // todo: check for errors
    modifier onlyExistsOwner(uint256 campaignId) {
        require(campaignExist(campaignId));
        Crowdsale crowdsale = Crowdsale(campaigns[campaignId].crowdsaleAddress);
        require(msg.sender == crowdsale.token().owner() ||
                msg.sender == owner);
        _;
    }

    // Constructor to set custom owner
    function ICOPool(address _owner) public {
        require(_owner != 0);
        owner = _owner;
    }

    // Return true if campaign exists
    function campaignExist(uint256 campaignId) public constant returns (bool) {
        if (campaignIndex.length == 0) return false;
        return (campaignIndex[campaigns[campaignId].index] == campaignId);
    }

    // Adds new campaign
    function addCampaign(uint256 campaignId, uint256 rate, address crowdsaleAddress, string name)
            public returns (bool) {
        require(crowdsaleAddress != 0);     // crowdsale address cannot be zero
        require(rate != 0); // rate cannot be zero
        require(!campaignExist(campaignId)); // campaign must be new
        campaigns[campaignId].rate = rate;
        campaigns[campaignId].crowdsaleAddress = crowdsaleAddress;
        campaigns[campaignId].name = name;
        campaigns[campaignId].index = campaignIndex.push(campaignId) - 1;
        return true;
    }

    // Returns info about campaign
    function getCampaign(uint256 campaignId) public constant returns (uint256 rate, address crowdsaleAddress, string name) {
        require(campaignExist(campaignId));
        return (campaigns[campaignId].rate,
                campaigns[campaignId].crowdsaleAddress,
                campaigns[campaignId].name);
    }

    // Deletes campaign
    function deleteCampaign(uint256 campaignId)  public returns (bool) {
        uint rowToDelete = campaigns[campaignId].index;
        uint256 keyToMove = campaignIndex[campaignIndex.length - 1];
        campaignIndex[rowToDelete] = keyToMove;
        campaigns[keyToMove].index = rowToDelete;
        campaignIndex.length--;
        CampaignWasDeleted(campaignId);
        return true;
    }

    // Updates start time
    function updateStartTime(uint256 campaignId, uint256 newTime)  public returns (bool) {
        campaigns[campaignId].startTime = newTime;
        CampaignStartTimeWasUpdated(campaignId, newTime);
        return true;
    }
    // Updates end time
    function updateEndTime(uint256 campaignId, uint256 newTime)  public returns (bool) {
        campaigns[campaignId].endTime = newTime;
        CampaignEndTimeWasUpdated(campaignId, newTime);
        return true;
    }

    // Buys token from chosen crowdsale
    function buyTokens(uint256 campaignId) public payable returns (bool) {
        require(msg.value > 0);
        require(campaignExist(campaignId));
        uint256 fee = msg.value.mul(1).div(100);
        uint256 valueToInvest = msg.value - fee;
        owner.transfer(fee);
        return campaigns[campaignId].crowdsaleAddress.send(valueToInvest);
    }
}
